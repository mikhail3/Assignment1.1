package chess;

public enum Note {
    EATS,
    DOESNT_EAT,
    NO_NOTE;
}