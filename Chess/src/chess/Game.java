package chess;
import java.util.*;

import chess.pieces.Bishop;
import chess.pieces.ChessPiece;
import chess.pieces.King;
import chess.pieces.Knight;
import chess.pieces.Pawn;
import chess.pieces.Queen;
import chess.pieces.Rook;

public class Game {
	public Board 		 board;
	public String 	     turn;

	public Game() {
		this.board = new Board();
		this.turn  = "white";
	}

	// Starts the standard game
	public void setStandardLayout() {
		this.setPawns(1, "white");
		this.setPawns(6, "black");
		this.setRooks();
		this.setBishops();
		this.setKnights();
		this.setKingsAndQueens();
		System.out.println("Start Game");
	}

	// Resume the game from some state
	public void resumeGame(LinkedList<ChessPiece> figures, String turn) {
		this.turn = turn;
		for (int i = 0; i < figures.size(); i++) {
            if (!this.board.addChessPiece(figures.get(i))) {
            	throw new java.lang.Error("Two figures at the same spot or the coordinates out of range!");
            }
        }
	}

    /*------------------------<Helper functions>------------------------*/
	
	public void setPawns(int y, String color) {
		for (int x = 0; x < this.board.shape; x++) {
			ChessPiece ch = new Pawn( new Coordinates(x,y), color);
            if (!this.board.addChessPiece(ch)) {
            		throw new java.lang.Error("Pawn can not be set!");
            }
		}
	}
	
	public void setRooks() {
		ChessPiece rooks[] = new ChessPiece[4];
		rooks[0] = new Rook( new Coordinates(0,7), "black");
		rooks[1] = new Rook( new Coordinates(7,7), "black");
		rooks[2] = new Rook( new Coordinates(0,0), "white");
		rooks[3] = new Rook( new Coordinates(7,0), "white");
		this.addFourPieces(rooks, "Rooks");
	}
	
	public void setBishops() {
		ChessPiece bishops[] = new ChessPiece[4];
		bishops[0] = new Bishop( new Coordinates(2,7), "black");
		bishops[1] = new Bishop( new Coordinates(5,7), "black");
		bishops[2] = new Bishop( new Coordinates(2,0), "white");
		bishops[3] = new Bishop( new Coordinates(5,0), "white");
		this.addFourPieces(bishops, "Bishops");
	}
	
	public void setKnights() {
		ChessPiece knights[] = new ChessPiece[4];
		knights[0] = new Knight( new Coordinates(1,7), "black");
		knights[1] = new Knight( new Coordinates(6,7), "black");
		knights[2] = new Knight( new Coordinates(1,0), "white");
		knights[3] = new Knight( new Coordinates(6,0), "white");
		this.addFourPieces(knights, "Knights");		
	}
	
	public void setKingsAndQueens() {
		ChessPiece monarchs[] = new ChessPiece[4];
		monarchs[0] = new Queen( new Coordinates(3,7), "black");
		monarchs[1] = new King(  new Coordinates(4,7), "black");
		monarchs[2] = new Queen( new Coordinates(3,0), "white");
		monarchs[3] = new King(  new Coordinates(4,0), "white");
		this.addFourPieces(monarchs, "Monarchs");
	}
	
	public void addFourPieces(ChessPiece[] chesses, String chessname) {
		for (int i = 0; i < 4; i++) {
			if (!this.board.addChessPiece(chesses[i])) {
        			throw new java.lang.Error(chessname + " can not be set!");
			}
		}
	}
	
	public String toggleTurn() {
		if (this.turn == "white") {
			return "black";
		}
		return "white";
	}

	public void changeTurn() {
		this.turn = this.toggleTurn();
	}
	/*------------------------------------------------------------------*/

	public static void main(String args[]) {
		//Game g = new Game();
		//g.setStandardLayout();
		//System.out.println(g.board);
	}

}