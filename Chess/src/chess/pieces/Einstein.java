package chess.pieces;
import java.util.*;
import chess.Coordinates;
import chess.Move;
import chess.Note;

/* Einstein is capable to teleport to the point if he has enough energy to reach it*/

public class Einstein extends ChessPiece {
	private int energy;

	public Einstein(Coordinates coord, String color) {
		super(coord, color);
		this.type = "Einstein";
		this.canJumpOverPieces = true;
		this.energy = 10;
	}
	public Einstein(ChessPiece chess) {
		super(chess);
	}

	public boolean isMoveValidForFigure(Move move) {
		float m = Math.abs(this.coord.x - move.new_coord.x);
		float c = Math.abs(this.coord.y - move.new_coord.y);
		
		// Move is valid if Einstein has enough energy to reach the destination E >= mc^2 :)
		boolean hasEnoughEnergy = (energy >= (m*c*c));
		
		if (hasEnoughEnergy) {
			this.energy -= m*c*c;
			return true;
		}
		
		return false;
	}
	
	public LinkedList<Move> getPossibleMoves(int shape) {
		Note note = Note.NO_NOTE;
		LinkedList<Move> possibleMoves = new LinkedList<Move>();

		for (int x = 0; x < shape; x ++) {
			for (int y = 0; y < shape; y ++) {
				float m = Math.abs(this.coord.x - x);
				float c = Math.abs(this.coord.y - y);
				if (this.energy >= (m*c*c)) {
					possibleMoves.add(new Move(this.coord, new Coordinates(x, y), note));
				}
			}
		}

		return possibleMoves;
	}
}