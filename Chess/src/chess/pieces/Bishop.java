package chess.pieces;
import java.util.*;

import chess.Coordinates;
import chess.Move;

public class Bishop extends ChessPiece {
	public Bishop(Coordinates coord, String color) {
		super(coord, color);
		this.type = "Bishop";
	}

	public Bishop(ChessPiece chess) {
		super(chess);
	}

	public boolean isMoveValidForFigure(Move move) {
		// Checks whether the suggested coordinates are on the same diagonal with the bishop
		return (Math.abs(this.coord.x - move.new_coord.x) == Math.abs(this.coord.y - move.new_coord.y));
	}
	public LinkedList<Move> getPossibleMoves(int shape){
		LinkedList<Move> possibleMoves = new LinkedList<Move>();
		
		possibleMoves = this.addMovesOnSameDiagonal(possibleMoves, shape);

		return possibleMoves;
	}
}