package chess.pieces;

import chess.Coordinates;
import chess.Move;
import java.util.*;

public class ChessPieceInst extends ChessPiece {
	public ChessPieceInst(Coordinates coord) {
		super(coord, "white");
		this.type =  "Helper";
	}
	public boolean isMoveValidForFigure(Move move) {
		return true;
	}
	public LinkedList<Move> getPossibleMoves(int shape) {
		return new LinkedList<Move>();
	}
}