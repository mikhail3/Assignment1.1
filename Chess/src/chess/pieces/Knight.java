package chess.pieces;
import java.util.*;

import chess.Coordinates;
import chess.Move;
import chess.Note;

public class Knight extends ChessPiece {
	public Knight(Coordinates coord, String color) {
		super(coord, color);
		this.type = "Knight";
		this.canJumpOverPieces = true;
	}

	public Knight(ChessPiece chess) {
		super(chess);
	}

	public boolean isMoveValidForFigure(Move move) {
		boolean along_x = (Math.abs(this.coord.x - move.new_coord.x) == 2) && (Math.abs(this.coord.y - move.new_coord.y) == 1);
		boolean along_y = (Math.abs(this.coord.x - move.new_coord.x) == 1) && (Math.abs(this.coord.y - move.new_coord.y) == 2);
		return (along_x || along_y);
	}
	public LinkedList<Move> getPossibleMoves(int shape){
		LinkedList<Move> possibleMoves = new LinkedList<Move>();
		Note note = Note.NO_NOTE;
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x + 2, this.coord.y + 1), note));
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x + 2, this.coord.y - 1), note));
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x - 2, this.coord.y + 1), note));
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x - 2, this.coord.y - 1), note));
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x + 1, this.coord.y + 2), note));
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x + 1, this.coord.y - 2), note));
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x - 1, this.coord.y + 2), note));
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x - 1, this.coord.y - 2), note));
		return possibleMoves;
	}
}