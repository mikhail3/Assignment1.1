package chess.pieces;
import java.util.*;

import chess.Coordinates;
import chess.Move;
import chess.Note;

public class Pawn extends ChessPiece {
	public Pawn(Coordinates coord, String color) {
		super(coord, color);
		this.type = "Pawn";
	}
	public Pawn(ChessPiece chess) {
		super(chess);
	}
	public boolean isMovingRight(Move move, int side, int lineForDoubleMove) {
		// usual step forward
		if (((move.new_coord.y - this.coord.y) == side*1) && (move.note == Note.DOESNT_EAT) && (this.coord.x == move.new_coord.x)) {
			return true;
		} 

		// step forward on 2 squares
		if (((move.new_coord.y - this.coord.y) == side*2) && (move.note == Note.DOESNT_EAT) && (this.coord.y == lineForDoubleMove)) {
			return true;
		}
		
		// eats different piece
		if (((move.new_coord.y - this.coord.y) == side*1) && (Math.abs(this.coord.x - move.new_coord.x) == 1) && (move.note == Note.EATS)) {
			return true;
		}
		return false;
	}

	public boolean isMoveValidForFigure(Move move) {
		if (this.color == "white") {
			return this.isMovingRight(move, 1,  1);
		}
		else {
			return this.isMovingRight(move, -1, 6);
		}
	}
	
	public LinkedList<Move> getPossibleMoves(int shape) {
		LinkedList<Move> possibleMoves = new LinkedList<Move>();
		Note note = Note.NO_NOTE;

		if (this.color == "white") {
			possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x    , this.coord.y + 1), note));
			possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x + 1, this.coord.y + 1), note));
			possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x - 1, this.coord.y + 1), note));
			possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x    , this.coord.y + 2), note));
		}
		else {
			possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x    , this.coord.y - 1), note));
			possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x + 1, this.coord.y - 1), note));
			possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x - 1, this.coord.y - 1), note));
			possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x    , this.coord.y - 2), note));
		}

		return possibleMoves;
	}
}