package chess.pieces;
import java.util.*;

import chess.Coordinates;
import chess.Move;

public class Helicopter extends ChessPiece {
	public Helicopter(Coordinates coord, String color) {
		super(coord, color);
		this.type = "Helicopter";
		this.canJumpOverPieces = true;
	}
	public Helicopter(ChessPiece chess) {
		super(chess);
	}
	public boolean isMoveValidForFigure(Move move) {
		boolean sameLine     = ((this.coord.x == move.new_coord.x) || (this.coord.y == move.new_coord.y));
		boolean sameDiagonal = (Math.abs(this.coord.x - move.new_coord.x) == Math.abs(this.coord.y - move.new_coord.y));
		return (sameLine || sameDiagonal);
	}
	public LinkedList<Move> getPossibleMoves(int shape) {
		LinkedList<Move> possibleMoves = new LinkedList<Move>();

		possibleMoves = this.addMovesOnSameLineLine(possibleMoves, shape);
		possibleMoves = this.addMovesOnSameDiagonal(possibleMoves, shape);

		return possibleMoves;
	}
}