package chess.pieces;
import java.util.*;

import chess.Coordinates;
import chess.Move;
import chess.Note;
public class King extends ChessPiece {
	public King(Coordinates coord, String color) {
		super(coord, color);
		this.type = "King";
	}
	public King(ChessPiece chess) {
		super(chess);
	}
	public boolean isMoveValidForFigure(Move move) { 
		return ((Math.abs(this.coord.x - move.new_coord.x) < 2) && (Math.abs(this.coord.y - move.new_coord.y) < 2));
	}
	public LinkedList<Move> getPossibleMoves(int shape) {
		LinkedList<Move> possibleMoves = new LinkedList<Move>();
		Note note = Note.NO_NOTE;
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x + 1, this.coord.y + 1), note));
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x + 1, this.coord.y    ), note));
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x + 1, this.coord.y - 1), note));
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x    , this.coord.y - 1), note));
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x - 1, this.coord.y - 1), note));
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x - 1, this.coord.y    ), note));
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x - 1, this.coord.y + 1), note));
		possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x    , this.coord.y + 1), note));

		return possibleMoves;
	}
}