package chess.pieces;
import java.util.*;

import chess.Coordinates;
import chess.Move;
import chess.Note;

import java.lang.*;

public abstract class ChessPiece {
	public Coordinates coord;
	public Boolean canJumpOverPieces = false;
	public String color = "white";
	public String type;

	// Standard constructor
	public ChessPiece(Coordinates coord, String color) {
		this.coord = coord;
		this.color = color;
	}

	// Deep copy constructor
	public ChessPiece(ChessPiece chess) {
		this.coord             = new Coordinates(chess.coord);
		this.canJumpOverPieces = chess.canJumpOverPieces;
		this.color             = chess.color;
		this.type              = chess.type;
	}

	// Validate whether the move is valid for the figure
	public abstract boolean isMoveValidForFigure(Move move);

	// Returns possible destinations of the figure with out validation of "rules"
	public abstract LinkedList<Move> getPossibleMoves(int shape);

	// Return trajectory (Can be overwritten for special cases [like Knight] if needed)
	// Default version returns diagonal or line coordinates of the move
	public LinkedList<Coordinates> getTrajectory(Move move) {

		LinkedList<Coordinates> trajectory = new LinkedList<Coordinates>();

		if (!this.isMoveValidForFigure(move)) {
			throw new java.lang.Error("Not a valid move for this chess piece!");
		}

		// if x-coordinates are equal
		if (move.old_coord.x == move.new_coord.x) {
			for (int i = move.old_coord.y+1; i < move.new_coord.y; i++) {
				trajectory.add(new Coordinates(move.old_coord.x, i));
			}
			return trajectory;
		}

		// if y-coordinates are equal
		if (move.old_coord.y == move.new_coord.y) {
			for (int i = move.old_coord.x+1; i < move.new_coord.x; i++) {
				trajectory.add(new Coordinates(i, move.old_coord.y));
			}
			return trajectory;
		}

        // if points are on the same diagonal
		int x_dir = Integer.signum(move.new_coord.x - move.old_coord.x);
		int y_dir = Integer.signum(move.new_coord.y - move.old_coord.y);

		int x = move.old_coord.x + x_dir;
		int y = move.old_coord.y + y_dir;

		while ((x != move.new_coord.x) && (y != move.new_coord.y)) {
			x += x_dir;
			y += y_dir;
			trajectory.add(new Coordinates(x, y));
		}

		return trajectory;
	}

	// Used for Rook and Queen
	public LinkedList<Move> addMovesOnSameLineLine(LinkedList<Move> possibleMoves, int shape) {

		Note note = Note.NO_NOTE;

		for (int i = 0; i < shape; i++) {

			if (i != this.coord.x) { // Possible moves along the x-axis
				possibleMoves.add(new Move(this.coord, new Coordinates(i, this.coord.y), note));
			}

			if (i != this.coord.y) { // Possible moves along the y-axis
				possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x, i), note));
			}
		}

		return possibleMoves;
	}

	// Used for Bishop and Queen
	public LinkedList<Move> addMovesOnSameDiagonal(LinkedList<Move> possibleMoves, int shape) {

		Note note = Note.NO_NOTE;

		for (int i = 1; i < shape; i++) {
			possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x + i, this.coord.y + i), note));
			possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x + i, this.coord.y - i), note));
			possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x - i, this.coord.y + i), note));
			possibleMoves.add(new Move(this.coord, new Coordinates(this.coord.x - i, this.coord.y - i), note));
		}

		return possibleMoves;
	}

	@Override
	public boolean equals(Object obj) {
		ChessPiece piece = (ChessPiece) obj;
		return (this.coord.equals(piece.coord));
	}
	@Override 
	public String toString(){
		return this.color + " " + this.type + " (" + this.coord.x + ", " + this.coord.y + ")";
	}

}