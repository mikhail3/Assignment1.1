package chess.pieces;
import java.util.*;

import chess.Coordinates;
import chess.Move;

public class Rook extends ChessPiece {
	public Rook(Coordinates coord, String color) {
		super(coord, color);
		this.type = "Rook";
	}

	public Rook(ChessPiece chess) {
		super(chess);
	}

	public boolean isMoveValidForFigure(Move move) {
		// Checks wheter the suggested coordinates are on the same line with the rook
		return ((this.coord.x == move.new_coord.x) || (this.coord.y == move.new_coord.y));
	}
	public LinkedList<Move> getPossibleMoves(int shape){
		LinkedList<Move> possibleMoves = new LinkedList<Move>();

		possibleMoves = this.addMovesOnSameLineLine(possibleMoves, shape);

		return possibleMoves;
	}
}