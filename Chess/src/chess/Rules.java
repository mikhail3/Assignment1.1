package chess;
import java.util.*;

import chess.pieces.ChessPiece;
import chess.pieces.ChessPieceInst;

import java.io.*;

public class Rules {

	// Checks whether the move is valid (does not check for game ending condition though)
    public static boolean isMoveValid(Board board, String turn, Move move) {

    		// Get the figures with the same coordinates form the list of the figures
    		ChessPiece piece_to_move = board.getChessPiece(new ChessPieceInst(move.old_coord));
		ChessPiece piece_to_kill = board.getChessPiece(new ChessPieceInst(move.new_coord));

		// Adds note whether the figure eats different figure or not 
		Move moveWithNote = Rules.addNoteToMove(move, piece_to_move, piece_to_kill);

		// New coordinates not in board?
		if (Rules.isNotInBoard(board, move)) {
			System.out.println("Destination out of board!");
			return false;
		}

		// No piece to move
		if (piece_to_move.equals(Board.NullChessPiece)) {
			System.out.println("No piece to move!");
			return false;
		}

		// Move does not follow the rules of the figure
		if (!piece_to_move.isMoveValidForFigure(moveWithNote)) {
			System.out.println("Move is invalid for the figure!");
			return false;
		}

		// Moving figure of the wrong color
		if (piece_to_move.color != turn) {
			System.out.println("Piece to move is of the wrong color");
			return false;
		}

		// Killing figure of the wrong color
		if ((!piece_to_kill.equals(Board.NullChessPiece)) && (piece_to_kill.color == piece_to_move.color)) {
			System.out.println("Killing figure of the wrong color");
			return false;
		}

		// Piece jumps over figures only if allowed
		if (!piece_to_move.canJumpOverPieces) {
			if (Rules.isJumpingOverPieces(board, move, piece_to_move)) {
				System.out.println("Piece jumps over figures and is not allowed to");
				return false;
			}
		}

		return true;
    }

    public static boolean isInCheck(Board board, String turn) {
    	ChessPiece king = board.getKing(turn);
    	ChessPiece piece;

    	// Check whether any of the moves of any figures of opposite color endanger the king
    	for (int i = 0; i < board.figures.size(); i++) {
    		piece = board.figures.get(i);

    		// Only interested of the figures of the opposite color
    		if (piece.color == turn) {
    			continue;
    		}

    		System.out.println("InCheck checks " + piece);

    		// Check whether any of the valid possible moves of enemy end with the coordinates of the king
    		LinkedList<Move> moves = piece.getPossibleMoves(board.shape);
    		for (int j = 0; j < moves.size(); j++) {
    			Move move = moves.get(j);

    			System.out.println("InCheck tests " + move + " of " + piece + " and validation results are " + Rules.isMoveValid(board, Rules.toggleTurn(turn), move));

    			if (Rules.isMoveValid(board, Rules.toggleTurn(turn), move) && move.new_coord.equals(king.coord)) {
    				return true;
    			}
    		} 
    	}

    	return false;
    }

    public static boolean isInStalemate(Board board, String turn) {

    	ChessPiece piece;

    	// Try all possible moves of figures of the "turn" color and check whether the king is in check after them
    	for (int i = 0; i < board.figures.size(); i++) {
    		piece = board.figures.get(i);

    		// Only interested of the figures of same color
    		if (piece.color != turn) {
    			continue;
    		}

    		// Try all possible moves and check wheher after some of them the king is not in the check
    		LinkedList<Move> moves = piece.getPossibleMoves(board.shape);
    		for (int j = 0; j < moves.size(); j++) {
    			Move move = moves.get(j);

    			System.out.println("isInStalemate: checking results of move " + move + " of piece " + piece);
    			
    			// Check whether the move is valid 
    			if (!Rules.isMoveValid(board, turn, move)) {
    				continue;
    			}

    			// Apply the move to the copy of the board and check whether the new board is in check
    			Board boardForTest = new Board(board); 
    			boardForTest.applyMoveToBoard(turn, move);

    			if (!Rules.isInCheck(boardForTest, turn)) {
    				System.out.println("isInStalemate: Good move found " + move + " of piece " + piece + "!!!!");
    				return false;
    			}
    		}
    	}

    	return true;
    }

    public static boolean isInMate(Board board, String turn) {
    	return (Rules.isInCheck(board, turn) && Rules.isInStalemate(board, turn)); 
    }

	public static boolean isNotInBoard(Board board, Move move) {
		return ((move.new_coord.x >= board.shape) || (move.new_coord.y >= board.shape) || (move.new_coord.x < 0) || (move.new_coord.y < 0));
	}

	public static boolean isJumpingOverPieces(Board board, Move move, ChessPiece chess) {
		// Coordinates the chess piece crosses while moving
		LinkedList<Coordinates> coords = chess.getTrajectory(move);

		// Checks whether any figure stands in the crossing coordinates
		for (int i = 0; i < coords.size(); i++) {
			if (board.figures.contains(new ChessPieceInst(coords.get(i)))) {
				System.out.println("Trajectory of " + move + " is " + coords);
				return true;
			}
		}
		return false;
	}

	public static Move addNoteToMove(Move move, ChessPiece piece_to_move, ChessPiece piece_to_kill) {
		if (piece_to_kill.equals(Board.NullChessPiece)) {
			move.note = Note.DOESNT_EAT;
		}
		else {
			move.note = Note.EATS;
		}
		return move;
	}

	public static String toggleTurn(String turn) {
		if (turn == "white") {
			return "black";
		}
		return "white";
	}
}