package chess;
public class Coordinates {
	public int x,y;
	public Coordinates(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public Coordinates(Coordinates coord){
		int x = coord.x;
		int y = coord.y;
		this.x = x;
		this.y = y;
	}
	public String toString(){
		return "(" + x + ", " + y + ")";
	}
	public boolean equals(Coordinates coord) {
		return ((this.x == coord.x) && (this.y == coord.y));
	}
}