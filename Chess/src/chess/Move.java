package chess;

public class Move {

	public Note note = Note.NO_NOTE;
	public Coordinates old_coord;
	public Coordinates new_coord;

	public Move(Coordinates old_coord, Coordinates new_coord) {
		this.old_coord = old_coord;
		this.new_coord = new_coord;
	}

	public Move(Coordinates old_coord, Coordinates new_coord, Note note) {
		this.old_coord = old_coord;
		this.new_coord = new_coord;
		this.note      = note;
	}

	public String toString(){
		return this.old_coord + " -> " + this.new_coord;
	}
}