package chess.test;
import chess.*;
import chess.pieces.Bishop;
import chess.pieces.ChessPiece;
import chess.pieces.King;
import chess.pieces.Knight;
import chess.pieces.Pawn;
import chess.pieces.Queen;
import chess.pieces.Rook;
import chess.pieces.ChessPiece.*;

import java.util.*;
import junit.framework.*;
import junit.extensions.*;
import java.lang.AssertionError;
import java.io.*;

public class ChessTests extends TestCase {

	public static Test suite() {
		TestSuite testSuite = new TestSuite(ChessTests.class);
		return testSuite;
	}
	
	public void testAddFigure() {
		Board b = new Board();
		b.addChessPiece(new King( new Coordinates(0,0), "white"));
		
		assertTrue("AddFigure: ", b.figures.contains(new King( new Coordinates(0,0), "white")));
	}
	
	public void testPawnMovement() {
		ChessPiece ch = new Pawn( new Coordinates(0,1), "white");
		Move m = new Move(new Coordinates(0,1), new Coordinates(0,2), Note.EATS);
		Move m1 = new Move(new Coordinates(0,1), new Coordinates(0,2), Note.DOESNT_EAT);
		Move m2 = new Move(new Coordinates(0,1), new Coordinates(1,2), Note.DOESNT_EAT);
		assertFalse(ch.isMoveValidForFigure(m));
		assertTrue(ch.isMoveValidForFigure(m1));
		assertFalse(ch.isMoveValidForFigure(m2));
	}
	
	public void testBishopMovement() {
		ChessPiece ch = new Bishop( new Coordinates(0,1), "white");
		Move m = new Move(new Coordinates(0,1), new Coordinates(0,2), Note.EATS);
		Move m1 = new Move(new Coordinates(0,1), new Coordinates(2,3), Note.DOESNT_EAT);
		Move m2 = new Move(new Coordinates(0,1), new Coordinates(0,0), Note.DOESNT_EAT);
		assertFalse(ch.isMoveValidForFigure(m));
		assertTrue(ch.isMoveValidForFigure(m1));
		assertFalse(ch.isMoveValidForFigure(m2));
	}
	
	public void testKingMovement() {
		ChessPiece ch = new King( new Coordinates(1,1), "white");
		Move m = new Move(new Coordinates(1,1), new Coordinates(7,7), Note.EATS);
		Move m1 = new Move(new Coordinates(1,1), new Coordinates(0,2), Note.DOESNT_EAT);
		Move m2 = new Move(new Coordinates(1,1), new Coordinates(1,4), Note.DOESNT_EAT);
		assertFalse(ch.isMoveValidForFigure(m));
		assertTrue(ch.isMoveValidForFigure(m1));
		assertFalse(ch.isMoveValidForFigure(m2));
	}
	
	public void testKnightMovement() {
		ChessPiece ch = new Knight( new Coordinates(1,1), "white");
		Move m = new Move(new Coordinates(1,1), new Coordinates(7,7), Note.EATS);
		Move m1 = new Move(new Coordinates(1,1), new Coordinates(3,2), Note.DOESNT_EAT);
		Move m2 = new Move(new Coordinates(1,1), new Coordinates(1,4), Note.DOESNT_EAT);
		assertFalse(ch.isMoveValidForFigure(m));
		assertTrue(ch.isMoveValidForFigure(m1));
		assertFalse(ch.isMoveValidForFigure(m2));
	}
	
	public void testQueenMovement() {
		ChessPiece ch = new Queen( new Coordinates(1,1), "white");
		Move m = new Move(new Coordinates(1,1), new Coordinates(6,7), Note.EATS);
		Move m1 = new Move(new Coordinates(1,1), new Coordinates(5,5), Note.DOESNT_EAT);
		Move m2 = new Move(new Coordinates(1,1), new Coordinates(5,4), Note.DOESNT_EAT);
		Move m3 = new Move(new Coordinates(1,1), new Coordinates(1,4), Note.DOESNT_EAT);
		assertFalse(ch.isMoveValidForFigure(m));
		assertTrue(ch.isMoveValidForFigure(m1));
		assertFalse(ch.isMoveValidForFigure(m2));
		assertTrue(ch.isMoveValidForFigure(m3));
	}
	
	public void testRookMovement() {
		ChessPiece ch = new Rook( new Coordinates(1,1), "white");
		Move m = new Move(new Coordinates(1,1), new Coordinates(1,7), Note.EATS);
		Move m1 = new Move(new Coordinates(1,1), new Coordinates(7,1), Note.DOESNT_EAT);
		Move m2 = new Move(new Coordinates(1,1), new Coordinates(2,3), Note.DOESNT_EAT);
		assertTrue(ch.isMoveValidForFigure(m));
		assertTrue(ch.isMoveValidForFigure(m1));
		assertFalse(ch.isMoveValidForFigure(m2));
	}

	public void testKingInCheck1() {
		Game g = new Game();
		LinkedList<ChessPiece> figures = new LinkedList<ChessPiece>();
		figures.add(new King( new Coordinates(0,0), "white"));
		figures.add(new King( new Coordinates(7,7), "black"));
		figures.add(new Rook( new Coordinates(0,5), "black"));
		g.resumeGame(figures, "white");

		assertTrue("KingInCheck1: ", Rules.isInCheck(g.board, "white"));
	}

	public void testKingInCheck2() {
		Game g = new Game();
		LinkedList<ChessPiece> figures = new LinkedList<ChessPiece>();
		figures.add(new King ( new Coordinates(5,6), "white"));
		figures.add(new King ( new Coordinates(6,6), "black"));
		figures.add(new Queen( new Coordinates(6,5), "white"));
		g.resumeGame(figures, "white");
		
		assertTrue("KingInCheck1: ", Rules.isInCheck(g.board, "black"));
	}

	public void testKingInCheck3() {
		Game g = new Game();
		LinkedList<ChessPiece> figures = new LinkedList<ChessPiece>();
		figures.add(new King ( new Coordinates(5,6), "white"));
		figures.add(new King ( new Coordinates(7,7), "black"));
		figures.add(new Queen( new Coordinates(6,5), "white"));
		g.resumeGame(figures, "white");
		
		assertFalse("Statemate: ", Rules.isInCheck(g.board, "black"));
	}

	public void testStalemate() {
		Game g = new Game();
		LinkedList<ChessPiece> figures = new LinkedList<ChessPiece>();
		figures.add(new King ( new Coordinates(5,6), "white"));
		figures.add(new King ( new Coordinates(7,7), "black"));
		figures.add(new Queen( new Coordinates(6,5), "white"));
		g.resumeGame(figures, "white");
		
		assertTrue("Statemate: ", Rules.isInStalemate(g.board, "black"));
	}

}