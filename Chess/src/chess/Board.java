package chess;
import java.util.*;

import chess.pieces.Bishop;
import chess.pieces.ChessPiece;
import chess.pieces.ChessPieceInst;
import chess.pieces.King;
import chess.pieces.Knight;
import chess.pieces.Pawn;
import chess.pieces.Queen;
import chess.pieces.Rook;

import java.lang.*;
import java.io.*;

/*
	-- We assume that the shape of the board is a square with side length of size "shape"
	-- We also will use coordinate system with:
	   X-coordinate from 0 to (shape-1) with 0 located on a1 and 7 on h1 in case shape == 8
	   Y-coordinate from 0 to (shape-1) with 0 located on a1 and 7 on a8 in case shape == 8
*/

public class Board {
 
 	public LinkedList<ChessPiece> figures; 
	public static ChessPiece NullChessPiece = new ChessPieceInst(new Coordinates(-1,-1));
	public int shape = 8;

	// Usual constructor
	public Board() {
		this.figures = new LinkedList<ChessPiece>();
	}

	// Deep copy of the board object
	public Board(Board board) {
		this.figures = new LinkedList<ChessPiece>();
		for (int i = 0; i < board.figures.size(); i++) {

			ChessPiece chess = board.figures.get(i);
			ChessPiece new_chess = Board.NullChessPiece;

			if (chess.type == "Rook") {
				new_chess = new Rook(chess);
			}
			if (chess.type == "Bishop") {
				new_chess = new Bishop(chess);
			}
			if (chess.type == "Knight") {
				new_chess = new Knight(chess);
			}
			if (chess.type == "King") {
				new_chess = new King(chess);
			}
			if (chess.type == "Queen") {
				new_chess = new Queen(chess);
			}
			if (chess.type == "Pawn") {
				new_chess = new Pawn(chess);
			}
			this.figures.add(new_chess);
		}

	}

	// Adds chess piece to the board
	public boolean addChessPiece(ChessPiece chess) {

		// Checks whether some figure already standing on the requested coordinates and that coordinates are in board
		if ((this.figures.indexOf(chess) == -1) && (chess.coord.x < this.shape) && (chess.coord.y < this.shape)) {
			this.figures.add(chess);
			return true;
		}
		return false;

	}

	// Returns the chess piece from board which stands on the same coordinates with "chess"
	public ChessPiece getChessPiece(ChessPiece chess) {
		int index = this.figures.indexOf(chess);
		if (index == -1) {
			return Board.NullChessPiece;
		}
		return this.figures.get(index);
	}

	// Returns the king of the "turn" color from the board
	public ChessPiece getKing(String turn) {
		ChessPiece chess;
		for (int i = 0; i < this.figures.size(); i++) {
			chess = this.figures.get(i);
			if ((chess.type == "King") && (chess.color == turn)) {
				return chess;
			}
		}
		return Board.NullChessPiece;
	}

	// Moves the piece to the new coordinates and removes the piece if needed
	public boolean applyMoveToBoard(String turn, Move move) {

		ChessPiece piece_to_move = this.getChessPiece(new ChessPieceInst(move.old_coord));
		ChessPiece piece_to_kill = this.getChessPiece(new ChessPieceInst(move.new_coord));

		if (! Rules.isMoveValid(this, turn, move)) {
			return false;
		}

		piece_to_move.coord = move.new_coord;
		this.figures.remove(piece_to_kill);
		return true;
	}

	public String toString(){
		String s = "";
		for (int i = 0; i < this.figures.size(); i++) {
			s += this.figures.get(i);
			s += "\n";
		}
		return s;
	}
}