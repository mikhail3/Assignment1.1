package chess.gui;
import javax.swing.JButton;

import chess.Board;
import chess.Coordinates;
import chess.pieces.ChessPiece;
import chess.pieces.ChessPieceInst;

import java.awt.Color;
import java.awt.Image;

import javax.swing.ImageIcon;

public class ChessButton extends JButton {
	ImageIcon chessLogo,colorLogo;

	public ChessButton(int index, Board board, boolean isBlack) {
		super();
		
		// Get 2 dim coordinates from index
		int y = index/board.shape;
		int x = index%board.shape;
		
		// Get chess piece, which stands on the button
		ChessPiece piece_on_button = board.getChessPiece(new ChessPieceInst(new Coordinates(x,y)));
		
		// Set background color
		this.setOpaque(true);
		if (isBlack) {
			this.setBackground(Color.BLACK);
		}
		else {
			this.setBackground(Color.WHITE);
		}
		
		// If no chess piece on the board put black label in case the button should be black 
		if ((piece_on_button.equals(Board.NullChessPiece)) && (isBlack)) {
			colorLogo = new ImageIcon(this.getClass().getResource("./logos/Black.png"));
			this.setIcon(colorLogo);
			return;
		}
		
		// If there is a chess piece on the button put the label on it
		if (!piece_on_button.equals(Board.NullChessPiece)) {
			String logo  = piece_on_button.color + piece_on_button.type + ".png";
			
			// Resize the logo so that they fit the buttons
			chessLogo    = new ImageIcon(this.getClass().getResource("./logos/" + logo));
			Image img    = chessLogo.getImage();
			Image newimg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
			chessLogo    = new ImageIcon(newimg);
			
			this.setIcon(chessLogo);
			return;
		}
	}
}