package chess.gui;
import javax.swing.JPanel;

import chess.Board;
import chess.Game;

import javax.swing.JFrame;
import java.awt.GridLayout;

public class ChessGUI extends JFrame{
	JPanel p = new JPanel();
	ChessButton button[];

	public static void main(String [] args) {
		// Standard layout
		chess.Game g = new Game();
		g.setStandardLayout(); // init board
		new ChessGUI(g.board);
	}

	public ChessGUI(Board board) {
		super("ChessGUI");
		
		int shape = board.shape;
		button = new ChessButton[shape*shape];
		
		setSize(400, 400);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		p.setLayout(new GridLayout(shape, shape));
		
		// Add buttons with logos
		for (int i = 0; i < (shape*shape); i++) {
			boolean isBlack = ((i % 2) == ((i/shape) % 2));
			button[i] = new ChessButton(i, board, isBlack);
			p.add(button[i]);
		}
		
		this.add(p);
		setVisible(true);
	}
}